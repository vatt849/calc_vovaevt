﻿namespace Calc
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnPlus = new System.Windows.Forms.Button();
			this.tbFirst = new System.Windows.Forms.TextBox();
			this.tbSecond = new System.Windows.Forms.TextBox();
			this.tbRavno = new System.Windows.Forms.TextBox();
			this.btnMinus = new System.Windows.Forms.Button();
			this.btnMulti = new System.Windows.Forms.Button();
			this.btnDiv = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnPlus
			// 
			this.btnPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnPlus.Location = new System.Drawing.Point(12, 54);
			this.btnPlus.Name = "btnPlus";
			this.btnPlus.Size = new System.Drawing.Size(46, 43);
			this.btnPlus.TabIndex = 0;
			this.btnPlus.Text = "+";
			this.btnPlus.UseVisualStyleBackColor = true;
			this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
			// 
			// tbFirst
			// 
			this.tbFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbFirst.Location = new System.Drawing.Point(130, 12);
			this.tbFirst.Multiline = true;
			this.tbFirst.Name = "tbFirst";
			this.tbFirst.Size = new System.Drawing.Size(111, 36);
			this.tbFirst.TabIndex = 1;
			this.tbFirst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFirst_KeyPress);
			// 
			// tbSecond
			// 
			this.tbSecond.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbSecond.Location = new System.Drawing.Point(12, 12);
			this.tbSecond.Multiline = true;
			this.tbSecond.Name = "tbSecond";
			this.tbSecond.Size = new System.Drawing.Size(111, 36);
			this.tbSecond.TabIndex = 2;
			this.tbSecond.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSecond_KeyPress);
			// 
			// tbRavno
			// 
			this.tbRavno.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.tbRavno.Cursor = System.Windows.Forms.Cursors.Hand;
			this.tbRavno.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.tbRavno.Location = new System.Drawing.Point(139, 52);
			this.tbRavno.Multiline = true;
			this.tbRavno.Name = "tbRavno";
			this.tbRavno.ReadOnly = true;
			this.tbRavno.Size = new System.Drawing.Size(81, 97);
			this.tbRavno.TabIndex = 3;
			this.tbRavno.Text = "=";
			// 
			// btnMinus
			// 
			this.btnMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnMinus.Location = new System.Drawing.Point(12, 103);
			this.btnMinus.Name = "btnMinus";
			this.btnMinus.Size = new System.Drawing.Size(46, 48);
			this.btnMinus.TabIndex = 4;
			this.btnMinus.Text = "-";
			this.btnMinus.UseVisualStyleBackColor = true;
			this.btnMinus.Click += new System.EventHandler(this.btnMinus_Click);
			// 
			// btnMulti
			// 
			this.btnMulti.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnMulti.Location = new System.Drawing.Point(77, 54);
			this.btnMulti.Name = "btnMulti";
			this.btnMulti.Size = new System.Drawing.Size(46, 43);
			this.btnMulti.TabIndex = 5;
			this.btnMulti.Text = "*";
			this.btnMulti.UseVisualStyleBackColor = true;
			this.btnMulti.Click += new System.EventHandler(this.btnMulti_Click);
			// 
			// btnDiv
			// 
			this.btnDiv.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnDiv.Location = new System.Drawing.Point(77, 103);
			this.btnDiv.Name = "btnDiv";
			this.btnDiv.Size = new System.Drawing.Size(46, 48);
			this.btnDiv.TabIndex = 6;
			this.btnDiv.Text = ":";
			this.btnDiv.UseVisualStyleBackColor = true;
			this.btnDiv.Click += new System.EventHandler(this.btnDiv_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(241, 159);
			this.Controls.Add(this.btnDiv);
			this.Controls.Add(this.btnMulti);
			this.Controls.Add(this.btnMinus);
			this.Controls.Add(this.tbRavno);
			this.Controls.Add(this.tbSecond);
			this.Controls.Add(this.tbFirst);
			this.Controls.Add(this.btnPlus);
			this.Name = "Form1";
			this.Text = "Кукулятор";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnPlus;
		private System.Windows.Forms.TextBox tbFirst;
		private System.Windows.Forms.TextBox tbSecond;
		private System.Windows.Forms.TextBox tbRavno;
		private System.Windows.Forms.Button btnMinus;
		private System.Windows.Forms.Button btnMulti;
		private System.Windows.Forms.Button btnDiv;
	}
}

