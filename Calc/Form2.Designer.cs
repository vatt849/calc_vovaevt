﻿namespace Calc
{
	partial class Form2
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Okno = new System.Windows.Forms.TextBox();
			this.Plus = new System.Windows.Forms.Button();
			this.Ravno = new System.Windows.Forms.Button();
			this.Minus = new System.Windows.Forms.Button();
			this.Multi = new System.Windows.Forms.Button();
			this.Div = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Okno
			// 
			this.Okno.Location = new System.Drawing.Point(36, 27);
			this.Okno.Multiline = true;
			this.Okno.Name = "Okno";
			this.Okno.Size = new System.Drawing.Size(224, 32);
			this.Okno.TabIndex = 0;
			this.Okno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Okno_KeyPress);
			// 
			// Plus
			// 
			this.Plus.Location = new System.Drawing.Point(36, 81);
			this.Plus.Name = "Plus";
			this.Plus.Size = new System.Drawing.Size(42, 44);
			this.Plus.TabIndex = 1;
			this.Plus.Text = "+";
			this.Plus.UseVisualStyleBackColor = true;
			this.Plus.Click += new System.EventHandler(this.Plus_Click);
			// 
			// Ravno
			// 
			this.Ravno.Location = new System.Drawing.Point(126, 131);
			this.Ravno.Name = "Ravno";
			this.Ravno.Size = new System.Drawing.Size(42, 44);
			this.Ravno.TabIndex = 2;
			this.Ravno.Text = "=";
			this.Ravno.UseVisualStyleBackColor = true;
			this.Ravno.Click += new System.EventHandler(this.Ravno_Click);
			// 
			// Minus
			// 
			this.Minus.Location = new System.Drawing.Point(97, 81);
			this.Minus.Name = "Minus";
			this.Minus.Size = new System.Drawing.Size(42, 44);
			this.Minus.TabIndex = 3;
			this.Minus.Text = "-";
			this.Minus.UseVisualStyleBackColor = true;
			this.Minus.Click += new System.EventHandler(this.Minus_Click);
			// 
			// Multi
			// 
			this.Multi.Location = new System.Drawing.Point(157, 81);
			this.Multi.Name = "Multi";
			this.Multi.Size = new System.Drawing.Size(42, 44);
			this.Multi.TabIndex = 4;
			this.Multi.Text = "*";
			this.Multi.UseVisualStyleBackColor = true;
			this.Multi.Click += new System.EventHandler(this.Multi_Click);
			// 
			// Div
			// 
			this.Div.Location = new System.Drawing.Point(218, 81);
			this.Div.Name = "Div";
			this.Div.Size = new System.Drawing.Size(42, 44);
			this.Div.TabIndex = 5;
			this.Div.Text = "/";
			this.Div.UseVisualStyleBackColor = true;
			this.Div.Click += new System.EventHandler(this.Div_Click);
			// 
			// Form2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(302, 266);
			this.Controls.Add(this.Div);
			this.Controls.Add(this.Multi);
			this.Controls.Add(this.Minus);
			this.Controls.Add(this.Ravno);
			this.Controls.Add(this.Plus);
			this.Controls.Add(this.Okno);
			this.Name = "Form2";
			this.Text = "Form2";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox Okno;
		private System.Windows.Forms.Button Plus;
		private System.Windows.Forms.Button Ravno;
		private System.Windows.Forms.Button Minus;
		private System.Windows.Forms.Button Multi;
		private System.Windows.Forms.Button Div;
	}
}