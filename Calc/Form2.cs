﻿using System;
using System.Windows.Forms;

namespace Calc
{
	public partial class Form2 : Form
	{
		int act = 0;
		double memory = 0;
		public Form2()
		{
			InitializeComponent();
		}

		private void Okno_KeyPress( object sender, KeyPressEventArgs e )
		{

			if ( !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8) )
			{
				if ( e.KeyChar != ',' || Okno.Text.IndexOf(",") != -1)
				{
					if ( e.KeyChar != '-' || Okno.Text.IndexOf("-") != -1)
					{
						e.Handled = true;
					}
				}
				}
			}

		private void Plus_Click( object sender, EventArgs e )
		{
			act = 1;
			double.TryParse(Okno.Text, out memory); //пытаемся пихнуть в память значение из текстбокса
			Okno.Text = "";
		}

		private void Minus_Click( object sender, EventArgs e )
		{
			act = 2;
			double.TryParse(Okno.Text, out memory); //пытаемся пихнуть в память значение из текстбокса
			Okno.Text = "";

		}

		private void Multi_Click( object sender, EventArgs e )
		{
			act = 3;
			double.TryParse(Okno.Text, out memory); //пытаемся пихнуть в память значение из текстбокса
			Okno.Text = "";
		}

		private void Div_Click( object sender, EventArgs e )
		{
			act = 4;
			double.TryParse(Okno.Text, out memory); //пытаемся пихнуть в память значение из текстбокса
			Okno.Text = "";
		}
		private void Ravno_Click( object sender, EventArgs e )
		{
			double otvet = 0;

			//объявление переменной для хранения текущего значения текстбокса
			double current = act == 4 ? 1 : 0; //если act равен 4, то current = 1, иначе current = 0
			double.TryParse(Okno.Text, out current); //а вот здесь мы пытаемся пихнуть в куррент значение из текстбокса

			switch ( act )
			{
				case 1:
					otvet = memory + current;
					break;
				case 2:
					otvet = memory - current;
					break;
				case 3:
					otvet = memory * current;
					break;
				case 4:
					otvet = memory / current;
					break;
				default:
					MessageBox.Show("нехуй так делать!");
					MessageBox.Show("Совсем нехуй, смекаешь?", "Мазафакинг колькулятор", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					break;
			}
			Okno.Text = otvet.ToString();
			act = 0;
		}
	}
}
