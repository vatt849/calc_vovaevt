﻿using System;
using System.Windows.Forms;

namespace Calc
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		private void btnPlus_Click( object sender, EventArgs e )
		{
			double first;
			double second;
			double.TryParse(tbFirst.Text, out first);
			double.TryParse(tbSecond.Text, out second);
			tbRavno.Text = (first + second).ToString();
		}

		private void btnMinus_Click( object sender, EventArgs e )
		{
			double first;
			double second;
			double.TryParse(tbFirst.Text, out first);
			double.TryParse(tbSecond.Text, out second);
			tbRavno.Text = (first - second).ToString();
		}

		private void btnMulti_Click( object sender, EventArgs e )
		{
			double first;
			double second;
			double.TryParse(tbFirst.Text, out first);
			double.TryParse(tbSecond.Text, out second);
			tbRavno.Text = (first * second).ToString();

		}

		private void btnDiv_Click( object sender, EventArgs e )
		{
			double first;
			double second;
			double.TryParse(tbFirst.Text, out first);
			double.TryParse(tbSecond.Text, out second);
			tbRavno.Text = (first / second).ToString();

		}

		private void tbFirst_KeyPress( object sender, KeyPressEventArgs e )
		{
			if ( !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8) )
			{
				if ( e.KeyChar != ',' || tbFirst.Text.IndexOf(",") != -1 )
				{
					e.Handled = true;
				}
			}
		}

		private void tbSecond_KeyPress( object sender, KeyPressEventArgs e )
		{
			if ( !char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8) )
			{
				if ( e.KeyChar != ',' || tbFirst.Text.IndexOf(",") != -1 )
				{
					e.Handled = true;
				}
			}
		}

		private void Form1_Load( object sender, EventArgs e )
		{

		}
	}
}
